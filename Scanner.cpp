#include "nodes.h"
#include "Scanner.h"
#include "parser.hpp"

int ultimo = 0;
int pos = 0;
int token;
std::string palabra;
FILE *fichero;

typedef std::map<int, std::string> list;
list lista;
int equis = 1;


void abrir(char *NombreArchivo){
    fichero = fopen(NombreArchivo, "r");
    if(fichero == NULL) printf("ERROR AL ABRIR ARCHIVO\n\n");
}

void cerrar(){fclose(fichero);}

int yylex(){

    while(wsp());

    token = id();
    if(token != 500){
        yylval.Id = guardarEnLista(palabra);
        return token;
    }

    token = numeros();
    if(token != 500){
        yylval.value = atof(palabra.c_str());
        return token;
    }

    token = unique();
    if(token != 500) return token;

    if(eof()) return 0;

    return -1;
}

int guardarEnLista(std::string Id){
	int i;
	for(i = 1; i < equis; i++)
		if(lista[i] == Id) return i;

	lista[equis] = Id;
	equis += 1;
	return equis-1;
}

void tab(int n){
    for(int i = 0; i < n; i++) printf("    ");
}

void show(matrix &M, int t){

    if(M.n == 0){
            printf("ESTO NO ES POSIBLE :(\n");
            exit(0);
    }else{
        for(int i = 1; i <= M.n; i++){
            tab(t);
            for(int j = 1; j <= M.m; j++) printf("%.2f  ", M.data[i][j]);
            printf("\n");
        }
        printf("\n");
    }
}

matrix *Add(matrix *a, matrix *b){

    matrix *matriz = new matrix;
    table R;
    if(a -> n == b -> n && a -> m == b -> m){
        for(int i = 1; i <= a -> n; i++){
            for(int j = 1; j <= a -> m; j++){
                    R[i][j] = a ->data[i][j] + b ->data[i][j];
            }
        }
        matriz -> n = a -> n;
        matriz -> m  = a -> m;
        matriz -> data = R;
    }else{
        matriz -> n = 0;
        matriz -> m = 0;
    } return matriz;
}

matrix *Subs(matrix *a, matrix *b){

    matrix *matriz = new matrix;
    table R;
    if(a -> n == b -> n && a -> m == b -> m){
        for(int i = 1; i <= a -> n; i++){
            for(int j = 1; j <= a -> m; j++){
                    R[i][j] = a ->data[i][j] - b ->data[i][j];
            }
        }
        matriz -> n = a -> n;
        matriz -> m  = a -> m;
        matriz -> data = R;
    }else{
        matriz -> n = 0;
        matriz -> m = 0;
    }return matriz;
}

matrix *Mult(matrix *a, matrix *b){

    matrix *matriz = new matrix;
    table R;
    if(a -> m == b -> n){
        for(int i = 1; i <= a -> n; i++){
            for(int j = 1; j <= b -> m; j++){
                R[i][j] = 0;
                for(int k = 1; k <= a -> m;k++){
                    R[i][j] = R[i][j] + a -> data[i][k] * b -> data[k][j];
                }
            }
        }
        matriz -> n = a -> n;
        matriz -> m  = b -> m;
        matriz -> data = R;
    }else{
        matriz -> n = 0;
        matriz -> m = 0;
    }
    return matriz;
}

matrix *Evaluate(node *p, int t){

    matrix *matriz1 = new matrix;
    matrix *matriz2 = new matrix;

    if(!p) return NULL;
    switch(p -> type){
        case D:
            tab(t++);
            printf("?: ASIGNACION NO ENCONTRADA\n\n");
            return p -> M;

        case M:
            tab(t++);
            printf("M:\n");
            show(*p -> M, t);
            return p -> M;

        case add:
            tab(t++);
            printf("Add:\n");
            matriz1 = Evaluate(p ->left, t);
            matriz2 = Evaluate(p ->right, t);

            return Add(matriz1, matriz2);

        case sub:
            tab(t++);
            printf("Sub:\n");
            matriz1 = Evaluate(p ->left, t);
            matriz2 = Evaluate(p ->right, t);

            return Subs(matriz1, matriz2);

        case mult:
            tab(t++);
            printf("Mul:\n");
            matriz1 = Evaluate(p ->left, t);
            matriz2 = Evaluate(p ->right, t);

            return Mult(matriz1, matriz2);
    }
    return NULL;
}

bool wsp(){
    if(isspace(siguiente())){
        acepta();
        return true;
    }
    falla();
    return false;
}

int id(){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 3){
        anterior = actual;
        c = siguiente();

        switch(actual){
            case 0:
                if(c == '_' || isalpha(c)) actual = 1;
                else actual = 3;
                break;
            case 1:
                if(isalnum(c) || c == '_') actual = 1;
                else if(c == 39) actual = 2;
                else actual = 3;
                break;
            case 2:
                if(c == 39) actual = 2;
                else actual = 3;
                break;
        }
        if (actual != 3) palabra.push_back(c);
    }
    if(palabra == "throw"){
        retroceso();
        acepta();
        return 260;
    }else if(anterior == 1 || anterior == 2){
        retroceso();
        acepta();
        return 259;
    }else{
        falla();
        return 500;
    }
}

int numeros(){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 12 && actual != 13){
        anterior = actual;
        c = siguiente();

        switch(actual){
            case 0:
                if(c == '0') actual = 1;
                else if(c >= 49 && c <= 57) actual = 2;
                else actual = 13;
                break;
            case 1:
                if(c >= 48 && c <= 55) actual = 3;
                else if(c == 46) actual = 5;
                else if(c == 'x' || c == 'X') actual = 4;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
                break;
            case 2:
                if(isdigit(c)) actual = 2;
                else if(c == 46) actual = 5;
                else if(c == 'e' || c == 'E') actual = 6;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
                break;
            case 3:
                if(c >= 48 && c <= 55) actual = 3;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
                break;
            case 4:
                if(isdigit(c)) actual = 7;
                else if(c >= 65 && c <= 70) actual = 7;
                else if(c >= 97 && c <= 102) actual = 7;
                else actual = 13;
                break;
            case 5:
                if(isdigit(c)) actual = 8;
                else actual = 13;
                break;
            case 6:
                if(isdigit(c)) actual = 9;
                else if(c == 43 || c == 45) actual = 10;
                else actual = 13;
                break;
            case 7:
                if(isdigit(c)) actual = 11;
                else if(c >= 65 && c <= 70) actual = 11;
                else if(c >= 97 && c <= 102) actual = 11;
                else actual = 13;
                break;
            case 8:
                if(isdigit(c)) actual = 8;
                else if(c == 'e' || c == 'E') actual = 6;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
                break;
            case 9:
                if(isdigit(c)) actual = 9;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
                break;
            case 10:
                if(isdigit(c)) actual = 9;
                else actual = 13;
                break;
            case 11:
                if(isdigit(c)) actual = 7;
                else if(c >= 65 && c <= 70) actual = 7;
                else if(c >= 97 && c <= 102) actual = 7;
                else if((c >= 71 && c <= 90) || (c >= 103 && c <= 122)) actual = 12;
                else actual = 13;
                break;
        }
        if(actual != 13 && actual != 12) palabra.push_back(c);
    }
    if(actual != 12){
        if(anterior == 1 || anterior == 3 || anterior == 2 || anterior == 8 || anterior == 9 || anterior == 11){
            retroceso();
            acepta();
            return 258;
        }else{
            falla();
            return 500;
        }
    }else return -1;
}

int unique(){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 13){
        anterior = actual;
        c = siguiente();

        switch(actual){
            case 0:
                if(c == '(') {actual = 1; token = '(';}
                else if(c == ')') {actual = 2; token = ')';}
                else if(c == '[') {actual = 3; token = '[';}
                else if(c == ']') {actual = 4; token = ']';}
                else if(c == '+') {actual = 5; token = '+';}
                else if(c == '-') {actual = 6; token = '-';}
                else if(c == '*') {actual = 7; token = '*';}
                else if(c == '/') {actual = 8; token = '/';}
                else if(c == ',') {actual = 9; token = ',';}
                else if(c == ';') {actual = 10; token = ';';}
                else if(c == ':') {actual = 11; token = ':';}
                else if(c == '.') {actual = 12; token = '.';}
                else actual = 13;
                break;
            default:
                actual = 13;
                break;
        }
        if (actual != 13) palabra.push_back(c);
    }
    if(anterior != 13){
        retroceso();
        acepta();
        return token;
    }else{
        falla();
        return 500;
    }
}

bool eof(){
    if(siguiente() == EOF) return true;
    return false;
}

void acepta(){ultimo = pos;}

void falla(){pos = ultimo;}

char siguiente(){
    char c;
    fseek(fichero, pos, SEEK_SET);
    c = fgetc(fichero);
    pos++;
    return c;
}

void retroceso(){pos--;}

void imprimir(){std::cout<<palabra;}
