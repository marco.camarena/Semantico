#ifndef __Scanner_header_
#define __Scanner_haeder_

#include<cstdio>
#include<cstdlib>
#include<cctype>
#include<fstream>
#include<iostream>
#include<string>
#include <cstring>

bool wsp();
int id();
int numeros();
int unique();
bool eof();

void abrir(char *);
void cerrar();

void acepta();
void falla();
char siguiente();
void retroceso();
void imprimir();

int yylex();
int guardarEnLista(std::string );

void tab(int);
void show(matrix &, int);
matrix *Add(matrix *, matrix *);
matrix *Subs(matrix *, matrix *);
matrix *Mult(matrix *, matrix *);
matrix *Evaluate(node *, int);

#endif
