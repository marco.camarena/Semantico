#include "nodes.h"
#include "parser.hpp"
#include "Scanner.h"

extern int yyparse();

int main(){

    char nombre[15];

    printf("Nombre de Archivo: ");
    std::cin >> nombre;
    printf("\n");

    abrir(nombre);
    printf("-----------------------------------\n\n");

    if(!yyparse()) printf("ARCHIVO ACEPTADO :)\n");
    else printf("\nARCHIVO RECHAZADO :(\n");

    cerrar();
    return 0;
}
