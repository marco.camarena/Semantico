#ifndef __nodes_
#define __nodes_

#include<cstdio>
#include<map>

using namespace std;

typedef std::map<int,float> row;
typedef std::map<int,row> table;

typedef enum {add, sub, mult, M, D} tnode;

typedef struct {
    int n,m;
    table data;
}matrix;

typedef struct x{
    tnode type;
    matrix *M;
    x *left;
    x *right;
}node;



#endif // __nodes_
