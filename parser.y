%{

#include "nodes.h"

extern int yylex();

extern void show(matrix &, int);
extern matrix *Evaluate(node*, int);

node *creanode(tnode, matrix*);
node *creanode(tnode, node*, node*);
node *buscar(int);
void asignacion(int, node*);

int yyerror(const char *);

typedef std::map<int , node*> dicc;

matrix *matriz;
int n, m;

dicc diccionario;

%}

%union{
	float value;
	int Id;
	matrix *mptr;
	node *node;
}
	%token <value> _num
	%token <Id> _id
	%token _throw

	%type<mptr> throwing;
	%type<node> mexpression;
	%type<node> mterm;
	%type<node> mfactor;

	%type<mptr> mdefinition;
	%type<mptr> rowsequence;
	%type<mptr> row;

	%type<value> expression;
	%type<value> term;
	%type<value> factor;
%%

start: main {};

main:
		stsequence '.' {}
	;

stsequence:
		stsequence ';' statement {}
	|	statement {}
	;

statement:
		assignment {}
	|	throwing {
			printf("\nResultado: \n\n");
			show(*$1, 0);
			printf("-----------------------------------\n\n");
			}
	;

assignment:
		_id ':' mexpression {asignacion($1, $3);}
	;

throwing:
		_throw mexpression {$$ = Evaluate($2, 0);}
	;

mexpression:
		mexpression '+' mterm {$$ = creanode(add, $1, $3);}
	|	mexpression '-' mterm {$$ = creanode(sub, $1, $3);}
	|	mterm {$$ = $1;}
	;

mterm:
		mterm '*' mfactor {$$ = creanode(mult, $1, $3);}
	|	mfactor {$$ = $1;}
	;

mfactor:
		'(' mexpression ')' {$$ = $2;}
	|	mdefinition {$$ = creanode(M, $1);}
	|	_id {$$ = buscar($1);}
	;

mdefinition:
		'[' {
			matriz = new matrix;
			n = 1;
			m = 1;
		}
		rowsequence ']' {
				matriz -> n = n;
				matriz -> m = m-1;

				$$ = matriz;
				}
	;

rowsequence:
		rowsequence ';'  {
				n++;
				m = 1;
				}
				row {}
	|	row {}
	;

row:
		row ',' expression {
				matriz -> data[n][m] = $3;
				m++;
				}
	|	expression {
			matriz -> data[n][m] = $1;
			m++;
			}
	;

expression:
		expression '+' term {$$ = $1 + $3;}
	|	expression '-' term {$$ = $1 - $3;}
	|	term {$$ = $1;}
	;

term:
		term '*' factor {$$ = $1 * $3;}
	|	term '/' factor {
				if($3 == 0.0){
					yyerror("Division por cero\n");
					exit(0);
				}else
					$$ = $1 / $3;
				}
	|	factor {$$ = $1;}
	;

factor:
		'-' factor {$$ = - $2;}
	|	'(' expression ')' {$$ = $2;}
	|	_num {$$ = $1;}
	;
		
%%

node *creanode(tnode t, matrix *M){

	x *n = new x;

	n -> type = t;
	n -> M = M;
	n -> left = NULL;
	n -> right = NULL;

	return n;
}

node *creanode(tnode t, node *l, node *r){

	x *n = new x;

	n -> type = t;
	n -> M = NULL;
	n -> left = l;
	n -> right = r;

	return n;
}

node *buscar(int Id){

	if(diccionario[Id]){
		return diccionario[Id];
	}else{
		matrix *matriz = new matrix;
		matriz -> n = 0;
        	matriz -> m = 0;

		return creanode(D, matriz);
	}
}

void asignacion(int Id, node *p){
	diccionario[Id] = p;
}

int yyerror(const char* m){
	//puts("ERROR");
	printf("%s\n", m);
	return 0;
}
